package com.application.styleomega.styleomega.Activities;

import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import com.application.styleomega.styleomega.Fragments.CartFragment;
import com.application.styleomega.styleomega.Fragments.ContactFragment;
import com.application.styleomega.styleomega.Fragments.ProductsFragment;
import com.application.styleomega.styleomega.Fragments.ProductKidsFragment;
import com.application.styleomega.styleomega.Fragments.ProductMenFragment;
import com.application.styleomega.styleomega.Fragments.ProductWomenFragment;
import com.application.styleomega.styleomega.Fragments.UserDetailsFragment;
import com.application.styleomega.styleomega.Models.Product;
import com.application.styleomega.styleomega.Models.User;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import java.util.ArrayList;


public class HomeActivity extends AppCompatActivity{

    private SharedPreferencesHandler session;
    private static String currentUser;
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private static ArrayList<User> users;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Set a Toolbar to replace the ActionBar that is transparent in the theme file.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Products");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);;
        mActivityTitle = getTitle().toString();

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        setupDrawerContent(nvDrawer);

        //Get currently logged in user and store in a static variable for later use in the fragments
        session = new SharedPreferencesHandler(getApplicationContext());
        currentUser = session.getusename();

        //Generates list of users from the database cast to an ArrayList
        users = (ArrayList<User>) Product.listAll(User.class);

        //Initialises the Product Fragment to be the first thing users see when opening the app or logging in
        InitialiseFragment(ProductsFragment.class);

    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawer.setDrawerListener(mDrawerToggle);
    }

    //Ensures the drawer icon updates when clicked
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {

        //Ensures Drawer is created before onPostCreate is called
        setupDrawer();

        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }


    //Ensures the drawer icon remains the right icon even when the orientation changes or keyboard is brought up
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    //Code below pertains to Drawer View and Nav Menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Sets the action bar home action to toggle the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return false;
        }

        return super.onOptionsItemSelected(item);

    }

    //Code below pertains to Drawer View
    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
            new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {
                    selectDrawerItem(menuItem);
                    return true;
                }
            });
    }


    //Code below pertains to Drawer View
    public void selectDrawerItem(MenuItem menuItem) {

        // Create a new fragment and specify the fragment to show based on nav item clicked
        int id = menuItem.getItemId();
        Fragment fragment = null;
        Class fragmentClass = ProductsFragment.class;

        if (id == R.id.nav_products) {

            fragmentClass = ProductsFragment.class;

        }
        else if (id == R.id.nav_account) {

            fragmentClass = UserDetailsFragment.class;

        }  else if (id == R.id.nav_cart) {

            fragmentClass = CartFragment.class;

        } else if (id == R.id.nav_men) {

            fragmentClass = ProductMenFragment.class;

        } else if (id == R.id.nav_women) {

            fragmentClass = ProductWomenFragment.class;

        } else if (id == R.id.nav_kids) {

            fragmentClass = ProductKidsFragment.class;

        } else if (id == R.id.nav_contact) {

            fragmentClass = ContactFragment.class;

        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Inserts the fragment by replacing existing frame view
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);

        // Set action bar title to match that of current Fragment
        toolbar.setTitle(menuItem.getTitle());

        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    //Used to initialise fragments
    public void InitialiseFragment(Class fragmentName) {

        Fragment fragment = null;

        Class fragmentClass = fragmentName;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
    }


    //Overrides the back button so the user cannot navigate back to login after successfully logging in
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }


    //Method to end current activity
    void kill_activity() {
        finish();
    }
}