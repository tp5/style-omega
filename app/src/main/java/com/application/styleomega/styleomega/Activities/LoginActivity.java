package com.application.styleomega.styleomega.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.application.styleomega.styleomega.Models.User;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import java.util.List;


public class LoginActivity extends AppCompatActivity {

    private EditText UsernameLogin;
    private EditText PasswordLogin;
    private Button Login;
    private SharedPreferencesHandler session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Sets flag to make android turn the status bar colors to be black as well as the status bar transparent
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        UsernameLogin = (EditText)findViewById(R.id.etUsername);
        PasswordLogin = (EditText)findViewById(R.id.etPassword);
        Login = (Button)findViewById(R.id.btnLogin);

        session = new SharedPreferencesHandler(getApplicationContext());

        Login.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                validate(UsernameLogin.getText().toString(), PasswordLogin.getText().toString());
                session.setusename(UsernameLogin.getText().toString());
            }
        });
    }


    public void validate(String userName, String userPassword) {

        boolean loopCheck = false;
        List<User> users = User.listAll(User.class);

        for(User user : users){
            if(userName.equals(user.getUserName().toString()) && (userPassword.equals(user.getPassword().toString()))) {
                Toast.makeText(getApplicationContext(), "Login Successful, Welcome "+ user.getUserName(), Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(i);
                loopCheck = true;
                finish();
            }
        }

        if(loopCheck == false) {
            PasswordLogin.setText("");
            PasswordLogin.requestFocus();
            Toast.makeText(getApplicationContext(), "Incorrect username/password", Toast.LENGTH_LONG).show();
        }
    }
}
