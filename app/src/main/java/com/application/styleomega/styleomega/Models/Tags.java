package com.application.styleomega.styleomega.Models;

import com.orm.SugarRecord;


public class Tags extends SugarRecord {

    private Long Id;
    private String Name;

    public Tags() {

    }

    public Tags(Long id, String name) {
        Id = id;
        Name = name;
    }

    @Override
    public Long getId() {
        return Id;
    }

    @Override
    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}


