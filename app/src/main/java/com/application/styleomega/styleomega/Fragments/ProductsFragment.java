package com.application.styleomega.styleomega.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SearchView;
import com.application.styleomega.styleomega.Activities.ProductDetailsActivity;
import com.application.styleomega.styleomega.Adapters.ProductsAdapter;
import com.application.styleomega.styleomega.Models.Product;
import com.application.styleomega.styleomega.R;
import java.util.ArrayList;


public class ProductsFragment extends Fragment {

    private GridView productsGrid;
    private static ProductsAdapter productsAdapter;
    SearchView searchView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View myFragmentView = inflater.inflate(R.layout.fragment_products, container, false);

        //Generates list of products from the database and stores them in a List that is cast to an ArrayList
        ArrayList<Product> products = (ArrayList<Product>) Product.listAll(Product.class);

        searchView = (SearchView) myFragmentView.findViewById(R.id.searchProducts);
        searchView.setQueryHint("Search Products");

        productsAdapter = new ProductsAdapter(getContext(), products);
        productsGrid = (GridView) myFragmentView.findViewById(R.id.gvProducts);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                productsAdapter.getFilter().filter(query);
                return false;
            }
        });


        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchView.setIconified(false);
            }
        });


        productsGrid.setAdapter(productsAdapter);

        productsGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Product selectedProduct = (Product) parent.getItemAtPosition(position);
                Intent gridIntent = new Intent(getContext(), ProductDetailsActivity.class);
                gridIntent.putExtra("pos", Integer.parseInt(selectedProduct.getId() - 1L + ""));
                startActivity(gridIntent);
            }
        });

        return myFragmentView;
    }
}
