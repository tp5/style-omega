package com.application.styleomega.styleomega.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import com.application.styleomega.styleomega.Adapters.PurchaseHistoryAdapter;
import com.application.styleomega.styleomega.Models.PurchaseHistory;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import java.util.ArrayList;
import java.util.Collections;


public class PurchaseHistoryActivity extends AppCompatActivity {

    private ArrayList<PurchaseHistory> purchases;
    private ArrayList<PurchaseHistory> filterList;
    private GridView purchaseHistoryGrid;
    private SharedPreferencesHandler session;
    private String currentUser;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_history);

        //Sets title for toolbar and back click
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Purchase History");
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        //Get currently logged in user and store in a static variable for later use in the fragments
        session = new SharedPreferencesHandler(getApplicationContext());
        currentUser = session.getusename();

        purchases = (ArrayList<PurchaseHistory>) PurchaseHistory.listAll(PurchaseHistory.class);
        filterList = new ArrayList<>();

        for (PurchaseHistory purchase : purchases) {
            if (purchase.getUsername().equals(currentUser)) {
                filterList.add(purchase);
                Log.v("purchase", purchase.getProduct().getName());
            }
        }

        reverseList();

        PurchaseHistoryAdapter purchaseHistoryAdapter = new PurchaseHistoryAdapter(getApplicationContext(), filterList);

        //If the user has not made any purchases
        if (purchaseHistoryAdapter.getCount() == 0) {
            setContentView(R.layout.empty_database);

            TextView tvMessage = (TextView) findViewById(R.id.txtMessage);
            tvMessage.setText("No Items in cart");

        } else {

            purchaseHistoryGrid = (GridView) findViewById(R.id.gvPurchaseHistory);

            purchaseHistoryGrid.setAdapter(purchaseHistoryAdapter);
        }
    }

    private void reverseList() {

        Collections.reverse(filterList);
    }
}
