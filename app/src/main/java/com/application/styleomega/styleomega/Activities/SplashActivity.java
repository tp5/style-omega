package com.application.styleomega.styleomega.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import com.application.styleomega.styleomega.Models.Product;
import com.application.styleomega.styleomega.Models.ProductTags;
import com.application.styleomega.styleomega.Models.Tags;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class SplashActivity extends AppCompatActivity {

    private SharedPreferencesHandler session;
    private ArrayList<Product> products;
    private Tags t;
    private ProductTags pts;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        products = (ArrayList<Product>) Product.listAll(Product.class);

        //Generates product database at runtime, making sure to delete the old database beforehand
        if(products.isEmpty()) {
            genProducts();
            genTags();
            genProductTags();
        }

        session = new SharedPreferencesHandler(getApplicationContext());

        //Checks if user is logged in and takes them direct to the app, if not loads the main activity where they can login or register
        if(session.getusename().length() > 0){
            Intent i = new Intent(getApplicationContext(),HomeActivity.class);
            startActivity(i);
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
            finish();
    }


    public void genProducts() {

        Gson gson = new Gson();
        Type listType = new TypeToken<List<Product>>(){}.getType();
        final ArrayList<Product> productsGson = gson.fromJson(readFromFileProducts(), listType);
        SugarRecord.saveInTx(productsGson);
    }


    public void genTags() {

        Gson gson = new Gson();
        Type listType = new TypeToken<List<Tags>>(){}.getType();
        final ArrayList<Tags> tagsGson = gson.fromJson(readFromFileTags(), listType);
        SugarRecord.saveInTx(tagsGson);
    }


    public void genProductTags() {

        List<Product> products = Product.listAll(Product.class);
        List<Tags> tags = Tags.listAll(Tags.class);

        for(Product product : products) {
            for(Tags tag : tags) {
                if(product.getTags().contains(tag.getId().toString())) {
                    ProductTags pts = new ProductTags(product.getName(), tag.getName());
                    Log.v("productTag", pts.getProductName() + " " + pts.getTagName());
                    pts.save();
                }
            }
        }
    }


    private String readFromFileProducts() {

        StringBuffer sbJsonString = new StringBuffer();
        InputStream is = getResources().openRawResource(R.raw.products);

        int character;

        try {
            while((character = is.read()) !=-1) {
                sbJsonString.append((char) character);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sbJsonString.toString();
    }


    private String readFromFileTags() {

        StringBuffer sbJsonString = new StringBuffer();
        InputStream is = getResources().openRawResource(R.raw.tags);

        int character;

        try {
            while((character = is.read()) !=-1) {
                sbJsonString.append((char) character);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sbJsonString.toString();
    }
}
