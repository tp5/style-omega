package com.application.styleomega.styleomega.Models;

import com.orm.SugarRecord;


public class Cart extends SugarRecord {

    private Long product;
    private String username;
    private Integer cartQuantity;
    private boolean cartStatus;

    public Cart() {

    }

    public Cart(Long product, String username, Integer cartQuantity, boolean cartStatus) {
        this.product = product;
        this.username = username;
        this.cartQuantity = cartQuantity;
        this.cartStatus = cartStatus;
    }

    public Long getProduct() {
        return product;
    }

    public void setProduct(Long product) {
        this.product = product;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getCartQuantity() {
        return cartQuantity;
    }

    public void setCartQuantity(Integer cartQuantity) {
        this.cartQuantity = cartQuantity;
    }

    public boolean getCartStatus() {
        return cartStatus;
    }

    public void setCartStatus(boolean cartStatus) {
        this.cartStatus = cartStatus;
    }
}



