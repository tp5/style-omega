package com.application.styleomega.styleomega.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SearchView;
import com.application.styleomega.styleomega.Activities.ProductDetailsActivity;
import com.application.styleomega.styleomega.Adapters.ProductsAdapter;
import com.application.styleomega.styleomega.Models.Product;
import com.application.styleomega.styleomega.Models.ProductTags;
import com.application.styleomega.styleomega.R;
import java.util.ArrayList;
import java.util.List;


public class ProductMenFragment extends Fragment {

    private GridView productsGrid;
    private SearchView searchView;
    ArrayList<Product> Men;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View myFragmentView = inflater.inflate(R.layout.fragment_products, container, false);

        productsGrid = (GridView) myFragmentView.findViewById(R.id.gvProducts);

        List<Product> products = Product.listAll(Product.class);
        List<ProductTags> productTags = ProductTags.listAll(ProductTags.class);
        Men = new ArrayList<>();

        final ProductsAdapter productsMenAdapter = new ProductsAdapter(getContext(), Men);
        productsGrid.setAdapter(productsMenAdapter);

        //Queries for all products that are for men
        for(Product product : products) {
            for (ProductTags productTag : productTags) {
                if (productTag.getTagName().equals("Men")) {
                    if(productTag.getProductName().equals(product.getName())) {
                        Men.add(product);
                    }
                }
            }
        }

        searchView = (SearchView) myFragmentView.findViewById(R.id.searchProducts);

        productsGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Product selectedProduct = (Product) parent.getItemAtPosition(position);
                Intent gridIntent = new Intent(getContext(), ProductDetailsActivity.class);
                gridIntent.putExtra("pos", Integer.parseInt(selectedProduct.getId() - 1 + ""));
                startActivity(gridIntent);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                productsMenAdapter.getFilter().filter(query);
                return false;
            }

        });

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        return myFragmentView;
    }
}