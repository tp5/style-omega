package com.application.styleomega.styleomega.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.application.styleomega.styleomega.Activities.ChangePasswordActivity;
import com.application.styleomega.styleomega.Activities.MainActivity;
import com.application.styleomega.styleomega.Activities.PurchaseHistoryActivity;
import com.application.styleomega.styleomega.Activities.UpdateDetailsActivity;
import com.application.styleomega.styleomega.Models.User;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import java.util.List;


public class UserDetailsFragment extends Fragment {

    private SharedPreferencesHandler session;
    private static String currentUser;
    private TextView tvBalance;
    private Button btnChangePassword;
    private Button btnChangeDetails;
    private Button btnPurchaseHistory;
    private Button btnLogout;
    private User currentUserDetails;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Get currently logged in user and store in a static variable for later use in the fragments
        session = new SharedPreferencesHandler(getContext());
        currentUser = session.getusename();

        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_user_details, container, false);

        tvBalance = (TextView) fragmentView.findViewById(R.id.txtBalance);
        btnChangePassword = (Button) fragmentView.findViewById(R.id.btnChangePassword);
        btnChangeDetails = (Button) fragmentView.findViewById(R.id.btnChangeDetails);
        btnPurchaseHistory = (Button) fragmentView.findViewById(R.id.btnPurchaseHistory);
        btnLogout = (Button) fragmentView.findViewById(R.id.btnLogout);

        List<User> users = User.listAll(User.class);
        currentUserDetails = new User();

        for(User user : users) {
            if(user.getUserName().equals(currentUser)) {
                currentUserDetails.setUserName(user.getUserName());
                currentUserDetails.setEmail(user.getEmail());
                currentUserDetails.setfName(user.getfName());
                currentUserDetails.setlName(user.getlName());
                currentUserDetails.setPhoneNumber(user.getPhoneNumber());
                currentUserDetails.setBalance(user.getBalance());
            }
        }

        tvBalance.setText("$"+String.format("%.2f", currentUserDetails.getBalance()));

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });


        btnChangeDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), UpdateDetailsActivity.class);
                startActivity(intent);
            }
        });

        btnPurchaseHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), PurchaseHistoryActivity.class);
                startActivity(intent);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setusename("");
                session.setItem("");
                Intent i = new Intent(getContext(),MainActivity.class);
                startActivity(i);
                kill_activity();
            }
        });
        return fragmentView;
    }

    //Method to end current activity
    void kill_activity() {
        getActivity().finish();
    }
}