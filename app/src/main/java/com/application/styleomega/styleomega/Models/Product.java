package com.application.styleomega.styleomega.Models;

import com.orm.SugarRecord;
import java.math.BigDecimal;


public class Product extends SugarRecord {

    private Long Id;
    private String Name;
    private String ShortDescription;
    private String LongDescription;
    private BigDecimal Price;
    private Integer Quantity;
    private Boolean Active;
    private String Tags;
    private String ScaledImage;
    private String FullImage;

    public Product() {

    }

    public Product(Long id, String name, String shortDescription, String longDescription, BigDecimal price, Integer quantity, Boolean active, String tags, String scaledImage, String fullImage) {
        Id = id;
        Name = name;
        ShortDescription = shortDescription;
        LongDescription = longDescription;
        Price = price;
        Quantity = quantity;
        Active = active;
        Tags = tags;
        ScaledImage = scaledImage;
        FullImage = fullImage;
    }

    @Override
    public Long getId() {
        return Id;
    }

    @Override
    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getShortDescription() {
        return ShortDescription;
    }

    public void setShortDescription(String shortDescription) {
        ShortDescription = shortDescription;
    }

    public String getLongDescription() {
        return LongDescription;
    }

    public void setLongDescription(String longDescription) {
        LongDescription = longDescription;
    }

    public BigDecimal getPrice() {
        return Price;
    }

    public void setPrice(BigDecimal price) {
        Price = price;
    }

    public Integer getQuantity() {
        return Quantity;
    }

    public void setQuantity(Integer quantity) {
        Quantity = quantity;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }

    public String getTags() {
        return Tags;
    }

    public void setTags(String tags) {
        Tags = tags;
    }

    public String getScaledImage() {
        return ScaledImage;
    }

    public void setScaledImage(String scaledImage) {
        ScaledImage = scaledImage;
    }

    public String getFullImage() {
        return FullImage;
    }

    public void setFullImage(String fullImage) {
        FullImage = fullImage;
    }
}

