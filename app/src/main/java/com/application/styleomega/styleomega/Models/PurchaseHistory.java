package com.application.styleomega.styleomega.Models;

import com.orm.SugarRecord;
import java.math.BigDecimal;


public class PurchaseHistory extends SugarRecord{

    private Product product;
    private Integer quantity;
    private String username;
    private String date;
    private BigDecimal price;

    public PurchaseHistory() {

    }

    public PurchaseHistory(Product product, Integer quantity, String username, String date, BigDecimal price) {
        this.product = product;
        this.quantity = quantity;
        this.username = username;
        this.date = date;
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
