package com.application.styleomega.styleomega.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.application.styleomega.styleomega.R;


public class MainActivity extends AppCompatActivity {

    private Button Login;
    private Button Register;
    private TextView txtOmega;
    private ConstraintLayout constraintLayout;
    private boolean b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
                constraintLayout = findViewById(R.id.constraintLayout);
                constraintLayout.setPadding(0, 0, 0, 70);

        }

        String style = "Style";
        String omega = "Omega";

        txtOmega = (TextView)findViewById(R.id.txtOmega);

        Spannable spannable = new SpannableString(style + " " + omega);

        spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#920952")), style.length(), (style + omega).length()+1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        txtOmega.setText(spannable, TextView.BufferType.SPANNABLE);

        Button LoginButton = (Button) findViewById(R.id.btnSignIn);
        LoginButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(i);

            }
        });

        Button RegisterButton = (Button) findViewById(R.id.btnSignUp);
        RegisterButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(),RegisterActivity.class);
                startActivity(i);

            }
        });
    }

    //Overrides the back button so the user cannot navigate back to products after logout
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
