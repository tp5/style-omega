package com.application.styleomega.styleomega.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.application.styleomega.styleomega.Models.User;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import java.util.ArrayList;


public class UpdateDetailsActivity extends AppCompatActivity {

    private SharedPreferencesHandler session;
    private EditText etUsername;
    private EditText etEmail;
    private EditText etFname;
    private EditText etLname;
    private EditText etPhoneNumber;
    private Button btnSave;
    private String username;
    private User currentUser;
    private  Toolbar toolbar;
    private boolean userCheck;
    private ArrayList<User> users;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_details);

        //Sets title for toolbar and back click
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Update Details");
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        //Get currently logged in user and store in a static variable for later use in the fragments
        session = new SharedPreferencesHandler(this);
        username = session.getusename();

        users = (ArrayList<User>) User.listAll(User.class);

        for(User user: users) {
            if(user.getUserName().equals(username))
                currentUser = user;
        }

        etUsername = (EditText) findViewById(R.id.txtUsername);
        etEmail = (EditText) findViewById(R.id.txtEmail);
        etFname = (EditText) findViewById(R.id.txtFName);
        etLname = (EditText) findViewById(R.id.txtLName);
        etPhoneNumber = (EditText) findViewById(R.id.txtPhoneNumber);
        btnSave = (Button) findViewById(R.id.btnSave);

        etUsername.setText(currentUser.getUserName());
        etEmail.setText(currentUser.getEmail());
        etFname.setText(currentUser.getfName());
        etLname.setText(currentUser.getlName());
        etPhoneNumber.setText(currentUser.getPhoneNumber());

        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                updateProfile();
            }
        });
    }


    public void updateProfile() {

        userCheck = true;

        if(etUsername.getText().toString().equals("") || etEmail.getText().toString().equals("")
                || etFname.getText().toString().equals("") || etLname.getText().toString().equals("")
                    || etEmail.getText().toString().equals("") || etPhoneNumber.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "You cannot have blank fields", Toast.LENGTH_LONG).show();
                        userCheck = false;

        } else if (etUsername.getText().toString().equals(currentUser.getUserName()) && etEmail.getText().toString().equals(currentUser.getEmail()) &&
                    etFname.getText().toString().equals(currentUser.getfName()) && etLname.getText().toString().equals(currentUser.getlName()) &&
                        etPhoneNumber.getText().toString().equals(currentUser.getPhoneNumber())) {
                            Toast.makeText(getApplicationContext(), "No changes made", Toast.LENGTH_LONG).show();
                            userCheck = false;
        } else {

            //Check if Username is unchnged and if the username is already taken
            if ((username.equals(etUsername.getText().toString()))) {
                Toast.makeText(getApplicationContext(), "Username the same", Toast.LENGTH_LONG).show();
            } else {
                for (User user : users) {
                    if ((etUsername.getText().toString().equals(user.getUserName()))) {
                        Toast.makeText(getApplicationContext(), "Username already taken", Toast.LENGTH_LONG).show();
                        userCheck = false;
                        etUsername.requestFocus();
                        etUsername.setText("");
                    }
                }
            }

            //Makes sure phone number does not exceed 15 characters
            if(etPhoneNumber.getText().toString().length() > 15) {
                Toast.makeText(getApplicationContext(), "Phone number not valid", Toast.LENGTH_LONG).show();
                etPhoneNumber.requestFocus();
                etPhoneNumber.setText("");
                userCheck = false;
            }
            //Makes sure the phone number is not too short
            else if(etPhoneNumber.getText().toString().length() < 8) {
                Toast.makeText(getApplicationContext(), "Phone number not valid", Toast.LENGTH_LONG).show();
                etPhoneNumber.requestFocus();
                etPhoneNumber.setText("");
                userCheck = false;
            } else {

                //Ensures email is of correct format
                if (!(etEmail.getText().toString().contains("@") && etEmail.getText().toString().contains(".com"))) {
                    Toast.makeText(getApplicationContext(), "Invalid Email address", Toast.LENGTH_LONG).show();
                    etEmail.requestFocus();
                    userCheck = false;
                } else {

                    //Makes sure the first & last names only contains letters
                    if (!(isAlphabet(etFname.getText().toString()) && isAlphabet(etLname.getText().toString()))) {
                        Toast.makeText(getApplicationContext(), "First & Last name must only contain letters", Toast.LENGTH_LONG).show();
                        etFname.requestFocus();
                        etFname.setText("");
                        etLname.setText("");
                        userCheck = false;
                    }
                }
            }
        }


        if(userCheck) {
            currentUser.setUserName(etUsername.getText().toString());
            currentUser.setEmail(etEmail.getText().toString());
            currentUser.setfName(etFname.getText().toString());
            currentUser.setlName(etLname.getText().toString());
            currentUser.setPhoneNumber(etPhoneNumber.getText().toString());
            currentUser.save();
            Toast.makeText(getApplicationContext(), "User updated", Toast.LENGTH_LONG).show();

            session.setusename(etUsername.getText().toString());

            finish();
        }
    }


    public boolean isAlphabet(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if(!Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }
}
