package com.application.styleomega.styleomega.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.application.styleomega.styleomega.Models.PurchaseHistory;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;


public class PurchaseHistoryAdapter extends BaseAdapter{

    private Context mContext;
    private ArrayList<PurchaseHistory> purchases;
    private SharedPreferencesHandler session;

    public PurchaseHistoryAdapter(Context c , ArrayList<PurchaseHistory> purchases) {
        mContext = c;
        this.purchases = purchases;
    }

    @Override
    public int getCount() {
        return purchases.size();
    }

    @Override
    public Object getItem(int position) {
        return purchases.get(position);
    }

    @Override
    public long getItemId(int position) {
        return purchases.indexOf(getItem(position));
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView=inflater.inflate(R.layout.adapter_purchase_history_grid, null);
        }

        TextView tvName = (TextView) convertView.findViewById(R.id.grid_name);
        TextView tvDate = (TextView) convertView.findViewById(R.id.grid_date);
        TextView tvQuantity = (TextView) convertView.findViewById(R.id.grid_quantity);
        TextView tvPaid = (TextView) convertView.findViewById(R.id.grid_paid);
        ImageView img = (ImageView) convertView.findViewById(R.id.grid_image);

        tvName.setText(purchases.get(+position).getProduct().getName());
        tvDate.setText("Date: " + purchases.get(+position).getDate());
        tvQuantity.setText("Amount: " + purchases.get(+position).getQuantity().toString());
        tvPaid.setText("Paid " + String.valueOf(purchases.get(+position).getPrice()));
        Picasso.get().load(purchases.get(+position).getProduct().getScaledImage()).into(img);

        return convertView;
    }
}