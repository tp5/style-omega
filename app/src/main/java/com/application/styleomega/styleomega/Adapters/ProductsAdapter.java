package com.application.styleomega.styleomega.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.application.styleomega.styleomega.Models.Product;
import com.application.styleomega.styleomega.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;


public class ProductsAdapter extends BaseAdapter implements Filterable{
    private Context mContext;
    private ArrayList<Product> products;
    CustomFilter filter;
    ArrayList<Product> filterList;

    public ProductsAdapter(Context c , ArrayList<Product> products) {
        mContext = c;
        this.products = products;
        this.filterList=products;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return products.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView=inflater.inflate(R.layout.adapter_product_grid, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.grid_name);
        ImageView img = (ImageView) convertView.findViewById(R.id.grid_image);

        textView.setText(products.get(+position).getName());
        Picasso.get().load(products.get(+position).getScaledImage()).into(img);

        return convertView;
    }

    @Override
    public Filter getFilter() {
        // TODO Auto-generated method stub
        if(filter == null)
        {
            filter=new CustomFilter();
        }

        return filter;
    }

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                constraint = constraint.toString().toUpperCase();

                ArrayList<Product> filters = new ArrayList<Product>();

                for (int i = 0; i < filterList.size(); i++) {
                    if (filterList.get(i).getName().toUpperCase().contains(constraint) || filterList.get(i).getLongDescription().toUpperCase().contains(constraint)) {
                        Product p = new Product(filterList.get(i).getId(), filterList.get(i).getName(), filterList.get(i).getShortDescription(), filterList.get(i).getLongDescription(), filterList.get(i).getPrice(), filterList.get(i).getQuantity(), filterList.get(i).getActive(), filterList.get(i).getTags(), filterList.get(i).getScaledImage(), filterList.get(i).getFullImage());
                        filters.add(p);
                    }
                }

                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = filterList.size();
                results.values = filterList;

            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults results) {
            products = (ArrayList<Product>) results.values;
            notifyDataSetChanged();
        }
    }
}