package com.application.styleomega.styleomega.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import com.application.styleomega.styleomega.Adapters.CheckoutAdapter;
import com.application.styleomega.styleomega.Models.Cart;
import com.application.styleomega.styleomega.Models.Product;
import com.application.styleomega.styleomega.Models.PurchaseHistory;
import com.application.styleomega.styleomega.Models.User;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import com.orm.SugarRecord;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;

public class CheckoutActivity extends AppCompatActivity {

    private SharedPreferencesHandler session;
    private String currentUser;
    private ArrayList<Cart> cartItems;
    private BigDecimal total;
    private BigDecimal itemCost;
    private boolean notEnoughBalance;
    private boolean purchaseComplete;
    private TextView tvTotal;
    private TextView tvBalance;
    private Button btnPurchase;
    private ArrayList<User> users;
    private ArrayList<Product> products;
    private ArrayList<Cart> filterCart;
    private ArrayList<PurchaseHistory> purchased;
    private Toolbar toolbar;
    private CheckoutAdapter checkoutAdapter;
    private GridView cartGrid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        //Get currently logged in user and store in a static variable for later use in the fragments
        session = new SharedPreferencesHandler(getApplicationContext());
        currentUser = session.getusename();

        //Sets title for toolbar and back click
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Checkout");
        setSupportActionBar(toolbar);

        //Set toolbar to kill the current activity on back click
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        tvTotal = (TextView) findViewById(R.id.txtTotal);
        tvBalance = (TextView) findViewById(R.id.txtBalance);
        btnPurchase = (Button) findViewById(R.id.btnPurchase);
        filterCart = new ArrayList<Cart>();
        users = (ArrayList<User>) User.listAll(User.class);
        products = (ArrayList<Product>) Product.listAll(Product.class);
        purchased = new ArrayList<>();
        itemCost = BigDecimal.ZERO;
        total = BigDecimal.ZERO;
        notEnoughBalance = false;
        purchaseComplete = false;

        cartItems = (ArrayList<Cart>) Cart.listAll(Cart.class);

        //Get the users cart buy filtering based on criteria of 'active' and username being that of currently logged in user
        for (Cart cart : cartItems) {
            if (cart.getCartStatus()) {
                if (cart.getUsername().equals(currentUser)) {
                    filterCart.add(cart);
                }
            }
        }

        checkoutAdapter = new CheckoutAdapter(this, filterCart);

        //Checks if any products are in the cart if not shows message saying so
        if (checkoutAdapter.getCount() == 0) {
            setContentView(R.layout.empty_database);

            TextView tvMessage = (TextView) findViewById(R.id.txtMessage);
            tvMessage.setText("No Items in cart");

        } else {

            //Calculate the total based on the users cart by multiplying the price by quantity, whilst casting Integer to double
            for (Cart cart : filterCart) {
                itemCost = SugarRecord.findById(Product.class, cart.getProduct()).getPrice().multiply(new BigDecimal(cart.getCartQuantity()));
                total = total.add(itemCost);
            }

            tvTotal.setText("$" + String.format("%.2f", total));

            //Set user balance to TextView
            for (User user : users) {
                if (user.getUserName().equals(currentUser)) {
                    tvBalance.setText("$" + String.format("%.2f", user.getBalance()));
                }
            }

            cartGrid = (GridView) findViewById(R.id.gvDetails);
            cartGrid.setAdapter(checkoutAdapter);

            btnPurchase.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    for (Cart cart : filterCart) {
                        for (Product product : products) {
                            if (SugarRecord.findById(Product.class, cart.getProduct()).getName().equals(product.getName())) {
                                if (product.getQuantity().equals(0) || product.getQuantity() < 1) {
                                    Toast.makeText(getApplicationContext(), "Sorry but we do not currently have any " + product.getName() + " available", Toast.LENGTH_LONG).show();
                                } else {

                                    //Check if user has enough money before purchasing
                                    for (User user : users) {
                                        if (user.getUserName().equals(currentUser)) {
                                            if (user.getBalance().compareTo(total) < 0) {
                                                notEnoughBalance = true;
                                            }
                                        }
                                    }

                                    if (notEnoughBalance) {

                                        Toast.makeText(getApplicationContext(), "Insufficient funds for purchase", Toast.LENGTH_LONG).show();

                                    } else {

                                        if (SugarRecord.findById(Product.class, cart.getProduct()).getId() == product.getId()) {
                                            if (!purchased.contains(product)) {

                                                PurchaseHistory purchaseNew = new PurchaseHistory(product, cart.getCartQuantity(), currentUser, Calendar.getInstance().getTime().toString(), itemCost);
                                                purchased.add(purchaseNew);

                                                product.setQuantity(product.getQuantity() - cart.getCartQuantity());
                                                product.save();
                                            }
                                        }

                                        Intent intent = new Intent(CheckoutActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                        finish();

                                        purchaseComplete = true;
                                    }
                                }
                            }
                        }
                    }

                    if (!notEnoughBalance) {

                        //Resets cart to be empty
                        for (Cart cartAll : cartItems) {
                            if (cartAll.getCartStatus()) {
                                if (cartAll.getUsername().equals(currentUser)) {
                                    cartAll.setCartStatus(false);
                                    cartAll.save();
                                }
                            }
                        }

                        //Decreases user balance
                        for (User user : users) {
                            if (user.getUserName().equals(currentUser)) {
                                user.setBalance(user.getBalance().subtract(total));
                                user.save();
                            }
                        }

                        SugarRecord.saveInTx(purchased);
                    }

                    if (purchaseComplete) {
                        Toast.makeText(getApplicationContext(), "Purchase completed successfully ", Toast.LENGTH_LONG).show();
                    }
                }

            });
        }
    }
}
