package com.application.styleomega.styleomega.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import com.application.styleomega.styleomega.Activities.CheckoutActivity;
import com.application.styleomega.styleomega.Activities.ProductDetailsActivity;
import com.application.styleomega.styleomega.Adapters.CartAdapter;
import com.application.styleomega.styleomega.Models.Cart;
import com.application.styleomega.styleomega.Models.Product;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import com.orm.SugarRecord;
import java.util.ArrayList;


public class CartFragment extends Fragment {

    private GridView cartGrid;
    private SharedPreferencesHandler session;
    private static String currentUser;
    private ArrayList<Cart> cartItems;
    private CartAdapter cartAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Get currently logged in user and store in a static variable for later use in the fragments
        session = new SharedPreferencesHandler(getContext());
        currentUser = session.getusename();

        cartItems = (ArrayList<Cart>) Cart.listAll(Cart.class);

        ArrayList<Cart> filterCart = new ArrayList<Cart>();

        for (Cart cart : cartItems) {
            if (cart.getCartStatus()) {
                if (cart.getUsername().equals(currentUser)) {
                    filterCart.add(cart);
                }
            }
        }

        // Inflate the layout for this fragment
        View myFragmentView = inflater.inflate(R.layout.fragment_cart, container, false);

        cartAdapter = new CartAdapter(getContext(), filterCart);

        //Checks if any products are in the cart if not shows message saying so
        if (cartAdapter.getCount() == 0) {
            myFragmentView = inflater.inflate(R.layout.empty_database, container, false);

            TextView tvMessage = (TextView) myFragmentView.findViewById(R.id.txtMessage);
            tvMessage.setText("No Items in cart");

        } else {

            cartGrid = (GridView) myFragmentView.findViewById(R.id.gvCart);
            Button btnCheckout = myFragmentView.findViewById(R.id.btnCheckout);

            cartGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent gridIntent = new Intent(getContext(), ProductDetailsActivity.class);
                    Long cartPosition = SugarRecord.findById(Product.class, cartItems.get(+position).getProduct()).getId() - 1;
                    gridIntent.putExtra("pos", Integer.parseInt("" + cartPosition));
                    startActivity(gridIntent);
                }
            });

            btnCheckout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), CheckoutActivity.class);
                    startActivity(intent);
                }
            });

            cartGrid.setAdapter(cartAdapter);
        }

        return myFragmentView;
    }

}
