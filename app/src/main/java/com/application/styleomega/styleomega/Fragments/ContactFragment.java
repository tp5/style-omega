package com.application.styleomega.styleomega.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.application.styleomega.styleomega.R;


public class ContactFragment extends Fragment{

    private TextView tvSubject;
    private TextView tvDescription;
    private Button btnSubmit;
    private ImageView btnCall;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View myFragmentView = inflater.inflate(R.layout.fragment_contact, container, false);

        tvSubject = (TextView)myFragmentView.findViewById(R.id.txtSubject);
        tvDescription = (TextView)myFragmentView.findViewById(R.id.txtDescription);
        btnSubmit = (Button)myFragmentView.findViewById(R.id.btnSubmit);
        btnCall = (ImageView)myFragmentView.findViewById(R.id.btnCall);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvSubject.getText().toString().matches("")) {
                    Toast.makeText(getContext(), "Please enter a valid subject", Toast.LENGTH_LONG).show();
                } else if (tvDescription.getText().toString().matches("")) {
                    Toast.makeText(getContext(), "Please enter a valid message", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getContext(), "Message Sent successfully", Toast.LENGTH_LONG).show();
                }
            }
        });

        //Brings up the dialer with the default contact number
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+94763555120"));
                startActivity(intent);
            }
        });

        return myFragmentView;
    }
}
