package com.application.styleomega.styleomega.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.application.styleomega.styleomega.Models.Cart;
import com.application.styleomega.styleomega.Models.Product;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import com.orm.SugarRecord;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;


public class ProductDetailsActivity extends AppCompatActivity {

    private SharedPreferencesHandler session;
    private static String currentUser;
    private Toolbar toolbar;
    private Product activeProduct;
    private List<Product> products;
    private Button btnAddToCart;
    private ImageView imgForeImg;
    private Product currentProductDetails;
    private ArrayList<Cart> cartList;
    private ImageView btnShare;
    private int index;
    private TextView txtDescription;
    private TextView txtPrice;
    private TextView txtInStock;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null)
            index = bundle.getInt("pos");

        Product activeProduct = getSelectedProduct();

        //Get currently logged in user and store in a static variable for later use in the fragments
        session = new SharedPreferencesHandler(getApplicationContext());
        currentUser = session.getusename();

        imgForeImg = (ImageView)findViewById(R.id.imgProduct);
        btnAddToCart = (Button)findViewById(R.id.btnAddToCart);
        btnShare = (ImageView)findViewById(R.id.btnShare);

        Picasso.get().load(activeProduct.getFullImage()).into(imgForeImg);

        currentProductDetails = new Product();

        for(Product product : products) {
            if(product.getId().equals(activeProduct.getId())) {
                currentProductDetails.setName(product.getName());
                currentProductDetails.setActive(product.getActive());
                currentProductDetails.setFullImage(product.getFullImage());
                currentProductDetails.setId(product.getId());
                currentProductDetails.setLongDescription(product.getLongDescription());
                currentProductDetails.setShortDescription(product.getShortDescription());
                currentProductDetails.setPrice(product.getPrice());
                currentProductDetails.setQuantity(product.getQuantity());
                currentProductDetails.setScaledImage(product.getScaledImage());
            }
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(currentProductDetails.getName());
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        txtDescription = (TextView) findViewById(R.id.txtDescription);
        txtPrice = (TextView) findViewById(R.id.txtPrice);
        txtInStock = (TextView) findViewById(R.id.txtInStock);

        txtDescription.setText(currentProductDetails.getLongDescription());
        txtPrice.setText(currentProductDetails.getPrice().toString());
        txtInStock.setText(currentProductDetails.getQuantity().toString());

        btnAddToCart.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                cartList = (ArrayList<Cart>) Cart.listAll(Cart.class);

                boolean productExists = false;
                boolean outOfStock = false;

                for(Cart cart : cartList) {
                    if (SugarRecord.findById(Product.class, cart.getProduct()).getId().equals(getSelectedProduct().getId()) && cart.getCartStatus() && cart.getUsername().equals(currentUser)) {
                        productExists = true;
                    }
                }

                if (productExists) {
                    Toast.makeText(getApplicationContext(), "Product already exists in cart", Toast.LENGTH_LONG).show();
                } else {
                    if(getSelectedProduct().getQuantity() < 1) {
                        Toast.makeText(getApplicationContext(), "Sorry we are currently out of stock for this product", Toast.LENGTH_LONG).show();
                    } else {
                        Cart newItem = new Cart(getSelectedProduct().getId(), currentUser, 1, true);
                        Log.v("prodID", currentProductDetails.getId().toString());
                        SugarRecord.save(newItem);
                        Toast.makeText(getApplicationContext(), "Product Added to Cart", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = currentProductDetails.getScaledImage();
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Style Omega Product");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        Log.v("currentUser", currentUser);
    }

    private Product getSelectedProduct() {

        activeProduct = new Product();
        products = Product.listAll(Product.class);

        activeProduct = products.get(index);

        return activeProduct;
    }

}
