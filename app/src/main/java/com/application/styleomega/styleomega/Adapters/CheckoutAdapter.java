package com.application.styleomega.styleomega.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.application.styleomega.styleomega.Models.Cart;
import com.application.styleomega.styleomega.Models.Product;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import com.orm.SugarRecord;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;


public class CheckoutAdapter extends BaseAdapter{

    private Context mContext;
    private ArrayList<Cart> cartList;
    private SharedPreferencesHandler session;

    public CheckoutAdapter(Context c , ArrayList<Cart> cartList) {
        mContext = c;
        this.cartList = cartList;
    }

    @Override
    public int getCount() {
        return cartList.size();
    }

    @Override
    public Object getItem(int position) {
        return cartList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cartList.indexOf(getItem(position));
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView=inflater.inflate(R.layout.adapter_checkout__grid, null);
        }

        TextView tvName = (TextView) convertView.findViewById(R.id.grid_name);
        TextView tvQuantity = (TextView) convertView.findViewById(R.id.grid_quantity);
        TextView tvUnitPrice = (TextView) convertView.findViewById(R.id.grid_unit_price);
        ImageView img = (ImageView) convertView.findViewById(R.id.grid_image);

        tvName.setText(SugarRecord.findById(Product.class, cartList.get(+position).getProduct()).getName());
        tvQuantity.setText("In cart: " + String.valueOf(cartList.get(+position).getCartQuantity()));
        tvUnitPrice.setText("Unit Cost: $" + String.valueOf(SugarRecord.findById(Product.class, cartList.get(+position).getProduct()).getPrice()));
        Picasso.get().load(SugarRecord.findById(Product.class, cartList.get(+position).getProduct()).getScaledImage()).into(img);

        return convertView;
    }
}