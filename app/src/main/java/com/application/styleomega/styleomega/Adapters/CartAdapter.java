package com.application.styleomega.styleomega.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.application.styleomega.styleomega.Models.Cart;
import com.application.styleomega.styleomega.Models.Product;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import com.orm.SugarRecord;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class CartAdapter extends BaseAdapter{

    private Context mContext;
    private ArrayList<Cart> cartList;
    private SharedPreferencesHandler session;

    public CartAdapter(Context c , ArrayList<Cart> cartList) {
        mContext = c;
        this.cartList = cartList;
    }

    @Override
    public int getCount() {
        return cartList.size();
    }

    @Override
    public Object getItem(int position) {
        return cartList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cartList.indexOf(getItem(position));
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView=inflater.inflate(R.layout.adapter_cart_grid, null);
        }

        TextView tvName = (TextView) convertView.findViewById(R.id.grid_name);
        TextView tvDescription = (TextView) convertView.findViewById(R.id.grid_description);
        TextView tvPrice = (TextView) convertView.findViewById(R.id.grid_price);
        TextView tvQuantity = (TextView) convertView.findViewById(R.id.grid_quantity);
        ImageView img = (ImageView) convertView.findViewById(R.id.grid_image);
        ImageButton btnRemove = (ImageButton) convertView.findViewById(R.id.btnRemoveFromCart);
        ImageButton btnIncrease = convertView.findViewById(R.id.btnPlus);
        ImageButton btnDecrease = convertView.findViewById(R.id.btnMinus);

        tvName.setText(SugarRecord.findById(Product.class, cartList.get(+position).getProduct()).getName());
        tvDescription.setText(SugarRecord.findById(Product.class, cartList.get(+position).getProduct()).getShortDescription());
        tvPrice.setText(String.valueOf("$" + SugarRecord.findById(Product.class, cartList.get(+position).getProduct()).getPrice()));
        tvQuantity.setText(cartList.get(+position).getCartQuantity().toString());
        Picasso.get().load(SugarRecord.findById(Product.class, cartList.get(+position).getProduct()).getScaledImage()).into(img);

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cart cart = cartList.get(+position);
                cart.setCartStatus(false);
                cart.save();
                cartList.remove(cartList.get(+position));
                notifyDataSetChanged();
                Toast.makeText(mContext, "Removed Successfully", Toast.LENGTH_LONG).show();
            }
        });

        btnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cart cart = cartList.get(+position);

                if (cart.getCartQuantity() >= SugarRecord.findById(Product.class, cartList.get(+position).getProduct()).getQuantity()) {
                    Toast.makeText(mContext, "Sorry maximum number of items reached", Toast.LENGTH_LONG).show();
                } else {
                    cart.setCartQuantity(cartList.get(+position).getCartQuantity() + 1);
                    cart.save();
                    notifyDataSetChanged();
                }
            }
        });

        btnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Cart cart = cartList.get(+position);

                if (cart.getCartQuantity() == 1 || cart.getCartQuantity() < 2) {
                    Toast.makeText(mContext, "Cannot have less than 1 item", Toast.LENGTH_LONG).show();
                } else {
                    cart.setCartQuantity(cartList.get(+position).getCartQuantity() - 1);
                    cart.save();
                    notifyDataSetChanged();
                }
            }
        });

        return convertView;
    }
}