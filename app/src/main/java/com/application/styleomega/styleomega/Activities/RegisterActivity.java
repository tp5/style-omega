package com.application.styleomega.styleomega.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.application.styleomega.styleomega.Models.User;
import com.application.styleomega.styleomega.R;
import com.orm.SugarRecord;
import java.math.BigDecimal;
import java.util.List;


public class RegisterActivity extends AppCompatActivity {

    private EditText Username;
    private EditText Password;
    private EditText Email;
    private EditText FName;
    private EditText LName;
    private EditText PhoneNumber;
    private boolean userCheck;
    private List<User> users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Username = (EditText)findViewById(R.id.txtCurrentPassword);
        Password = (EditText)findViewById(R.id.txtPassword);
        Email = (EditText)findViewById(R.id.txtEmail);
        FName = (EditText)findViewById(R.id.txtFName);
        LName = (EditText)findViewById(R.id.txtLName);
        PhoneNumber = (EditText)findViewById(R.id.txtPhoneNumber);

        Button RegisterButton = (Button) findViewById(R.id.btnRegister);
        RegisterButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                userCheck = true;

                users = User.listAll(User.class);

                for (User user : users) {
                    if ((Username.getText().toString().equals(user.getUserName().toString()))) {
                        Toast.makeText(getApplicationContext(), "Username already taken", Toast.LENGTH_LONG).show();
                        userCheck = false;
                    }
                }

                //Ensures all fields are filled out
                if(Username.getText().toString().isEmpty() || Password.getText().toString().isEmpty() || Email.getText().toString().isEmpty() ||
                        FName.getText().toString().isEmpty() || LName.getText().toString().isEmpty() || PhoneNumber.getText().toString().isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please fill all fields", Toast.LENGTH_LONG).show();
                            userCheck = false;
                } else {

                    //Makes sure phone number does not exceed 15 characters
                    if(PhoneNumber.getText().toString().length() > 15) {
                        Toast.makeText(getApplicationContext(), "Phone number not valid", Toast.LENGTH_LONG).show();
                        PhoneNumber.requestFocus();
                        PhoneNumber.setText("");
                        userCheck = false;
                    }
                    //Makes sure the phone number is not too short
                    else if(PhoneNumber.getText().toString().length() < 8) {
                        Toast.makeText(getApplicationContext(), "Phone number not valid", Toast.LENGTH_LONG).show();
                        PhoneNumber.requestFocus();
                        PhoneNumber.setText("");
                        userCheck = false;
                    } else {

                        //Ensures email is of correct format
                        if (!(Email.getText().toString().contains("@") && Email.getText().toString().contains(".com"))) {
                            Toast.makeText(getApplicationContext(), "Invalid Email address", Toast.LENGTH_LONG).show();
                            Email.requestFocus();
                            Email.setText("");
                            userCheck = false;
                        } else {

                            //Makes sure the first & last names only contains letters
                            if (!(isAlphabet(FName.getText().toString()) && isAlphabet(LName.getText().toString()))) {
                                Toast.makeText(getApplicationContext(), "First & Last name must only contain letters", Toast.LENGTH_LONG).show();
                                FName.requestFocus();
                                FName.setText("");
                                LName.setText("");
                                userCheck = false;
                            }
                        }
                    }
                }

                if (userCheck == true) {
                    //Saves User to database
                    User user = new User(Username.getText().toString(), Password.getText().toString(), Email.getText().toString(), FName.getText().toString(), LName.getText().toString(), PhoneNumber.getText().toString(), BigDecimal.valueOf(500));
                    SugarRecord.save(user);

                    Toast.makeText(getApplicationContext(), "Registration Success, Welcome " + user.getfName(), Toast.LENGTH_LONG).show();

                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });
    }

    public boolean isAlphabet(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if(!Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }
}
