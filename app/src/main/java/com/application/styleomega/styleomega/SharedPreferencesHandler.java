package com.application.styleomega.styleomega;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class SharedPreferencesHandler {

    private SharedPreferences prefs;

    public SharedPreferencesHandler(Context cntx) {
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void setusename(String usename) {
        prefs.edit().putString("usename", usename).commit();
    }

    public void setItem(String item) {
            prefs.edit().putString("item", item).commit();
        }

    public String getusename() {
        String usename = prefs.getString("usename","");
        return usename;
    }

    public String getItem() {
        String item = prefs.getString("item","");
        return item;
    }
}
