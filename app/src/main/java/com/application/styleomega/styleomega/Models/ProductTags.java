package com.application.styleomega.styleomega.Models;

import com.orm.SugarRecord;


public class ProductTags extends SugarRecord {
    String productName;
    String tagName;

    public ProductTags() {

    }

    public ProductTags(String productName, String tagName) {
        this.productName = productName;
        this.tagName = tagName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
