package com.application.styleomega.styleomega.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.application.styleomega.styleomega.Models.User;
import com.application.styleomega.styleomega.R;
import com.application.styleomega.styleomega.SharedPreferencesHandler;
import java.util.ArrayList;

public class ChangePasswordActivity extends AppCompatActivity {

    private SharedPreferencesHandler session;
    private EditText etCurrentPassword;
    private EditText etNewPassword;
    private EditText etConfirmPassword;
    private Button btnSave;
    private String username;
    private User currentUser;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        //Sets title for toolbar and back click
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Update Details");
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        //Get currently logged in user and store in a static variable for later use in the fragments
        session = new SharedPreferencesHandler(this);
        username = session.getusename();

        ArrayList<User> users = (ArrayList<User>) User.listAll(User.class);

        for(User user: users) {
            if(user.getUserName().equals(username))
                currentUser = user;
        }

        etCurrentPassword = (EditText) findViewById(R.id.txtCurrentPassword);
        etNewPassword = (EditText) findViewById(R.id.txtNewPassword);
        etConfirmPassword = (EditText) findViewById(R.id.txtConfirmPassword);
        btnSave = (Button) findViewById(R.id.btnUpdatePassword);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile();
            }

        });
    }

    public void updateProfile() {
        if(etCurrentPassword.getText().toString().equals(currentUser.getPassword())) {
            if(etNewPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
                if(etNewPassword.getText().toString().equals(currentUser.getPassword())) {
                    Toast.makeText(getApplicationContext(), "New password cannot be the same as the old one", Toast.LENGTH_LONG).show();
                    etNewPassword.setText("");
                    etConfirmPassword.setText("");
                    etNewPassword.requestFocus();
                } else {
                    currentUser.setPassword(etNewPassword.getText().toString());
                    Toast.makeText(getApplicationContext(), "Password Updated", Toast.LENGTH_LONG).show();
                    currentUser.save();
                    finish();
                }
            }
        } else {
            Toast.makeText(getApplicationContext(), "Incorrect password", Toast.LENGTH_LONG).show();
            etNewPassword.setText("");
            etConfirmPassword.setText("");
            etCurrentPassword.setText("");
            etCurrentPassword.requestFocus();
        }
    }
}
